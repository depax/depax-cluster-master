# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.5] - 2018-10-24
### Added

- Added package script `prepublishOnly`.

## [1.0.4] - 2018-10-22
### Updated

- Updated dev-tools dependency.
- Updated README shields.

## [1.0.3] - 2018-10-19
### Added

- Included the [unlicense] license details.
- Added this CHANGELOG file.
- Implemented Dev Tools packge.
- Added to CircleCI.

### Updated

- Updated variousfiles to work with the dev-tools package.

## [1.0.2] - 2018-10-09
### Updated

- Removed the chalk module as a dependency.

## [1.0.1] - 2018-10-01
### Added

- Added an index file exporting all relevant entities.
- Added feature tests

### Updated

- Moved ClusterMaster class into the Lib sub folder.
- Updated README, and added usage details.

## [1.0.0] - 2018-10-01
### Added

- Added support for [logger].

### Updated

- Converted private package to public.
- Refactored package

[unlicense]: http://unlicense.org/
[logger]: https://bitbucket.org/depax/depax-logger
[1.0.0]: https://bitbucket.org/depax/depax-cluster-master/src/v1.0.0/
[1.0.1]: https://bitbucket.org/depax/depax-cluster-master/src/v1.0.1/
[1.0.2]: https://bitbucket.org/depax/depax-cluster-master/src/v1.0.2/
[1.0.3]: https://bitbucket.org/depax/depax-cluster-master/src/v1.0.3/
[1.0.4]: https://bitbucket.org/depax/depax-cluster-master/src/v1.0.4/
[1.0.5]: https://bitbucket.org/depax/depax-cluster-master/src/v1.0.5/
