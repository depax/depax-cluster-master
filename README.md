# Cluster Master

[![CircleCI](https://circleci.com/bb/depax/depax-cluster-master.svg?style=svg)](https://circleci.com/bb/depax/depax-cluster-master)
[![Todos](https://circleci.com/api/v1.1/project/bitbucket/depax/depax-cluster-master/latest/artifacts/0/shields/todos.svg)](#)
[![Features](https://circleci.com/api/v1.1/project/bitbucket/depax/depax-cluster-master/latest/artifacts/0/shields/features.svg)](#)
[![Coverage](https://circleci.com/api/v1.1/project/bitbucket/depax/depax-cluster-master/latest/artifacts/0/shields/features-coverage.svg)](#)
[![Documentation](https://img.shields.io/badge/documentation-🕮-brightgreen.svg?style=plastic)](https://circleci.com/api/v1.1/project/bitbucket/depax/depax-cluster-master/latest/artifacts/0/documentation/index.html)
[![Report](https://img.shields.io/badge/report-💣-brightgreen.svg?style=plastic)](https://circleci.com/api/v1.1/project/bitbucket/depax/depax-cluster-master/latest/artifacts/0/report)

## Installation

Install the package normally using NPM or Yarn.

```sh
yarn add @depax/cluster-master
```

## Usage

An entry point callback is provided which would define the initialization of the applications master and worker instances.

```js
import EntryPoint, { Builds } from "@depax/cluster-master";

EntryPoint("TheNameOfYourApplicationProcess", {
    // The options to pass to the worker and cluster controllers.
    build: process.env.NODE_ENV === "production" ? Builds.Live : Builds.Dev,
}, async (config, logger) => {
    // Initialize the worker, here you would create an instance of your application.
    const MyApp = require("./MyApp").default;
    await (new MyApp()).initialize();
}, async (config) => {
    // initialize the master, here we can provide the master logger which will recieve all the workers logs using @depax/logger.
    const logger = new ConsoleLogger("master");
    return logger;
});
```

The EntryPoint second argument is some options for managing the workers and master, the following is the default options;

```js
{
    /** The title of the main process. */
    title: string = "";

    /** The build type. */
    build: Builds = process.env.NODE_ENV === "production" ? Builds.Live : Builds.Dev;

    /** The maximum number of forks. */
    maxForks: number = os.cpus().length;

    /** Set to true to use only master if in dev mode. */
    singleWorkerDev: boolean = true;

    /** The delay before a new fork is created during the initialization. */
    initializeForkDelay: number = 0;

    /** The number of attempts we can respawn workers and fails, this will be multiplied by the max forks. */
    respawnAttempts: number = 5;

    /** The amount of time in ms before the attempts are cleared. */
    respawnAttemptsInterval: number = 1000;
}
```
