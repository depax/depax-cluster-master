@cluster-master @logging
Feature: Cluster logging

  Scenario: Should capture worker logs in the master log
    Given Create ClusterMaster using logging workers
     When I wait for 4 seconds
     Then I should have the following logs;
      """
      [{
          "args": {},
          "message": "Starting the master cluster and attempting to start 2 workers.",
          "type": "INFO"
      }, {
          "args": { "worker": 1 },
          "message": "Hello world, info from a worker.",
          "type": "INFO"
      }, {
          "args": { "worker": 1 },
          "message": "Hello world, warn from a worker.",
          "type": "WARN"
      }, {
          "args": { "worker": 1 },
          "message": "Hello world, error from a worker.",
          "type": "ERROR"
      }, {
          "args": { "worker": 1 },
          "message": "Hello world, fatal from a worker.",
          "type": "FATAL"
      }, {
          "args": { "worker": 1 },
          "message": "Hello world, debug from a worker.",
          "type": "DEBUG"
      }, {
          "args": {},
          "message": "ClusterMasterTest process has been killed.",
          "type": "WARN"
      }, {
          "args": { "worker": 2 },
          "message": "Hello world, info from a worker.",
          "type": "INFO"
      }, {
          "args": { "worker": 2 },
          "message": "Hello world, warn from a worker.",
          "type": "WARN"
      }, {
          "args": { "worker": 2 },
          "message": "Hello world, error from a worker.",
          "type": "ERROR"
      }, {
          "args": { "worker": 2 },
          "message": "Hello world, fatal from a worker.",
          "type": "FATAL"
      }, {
          "args": { "worker": 2 },
          "message": "Hello world, debug from a worker.",
          "type": "DEBUG"
      }, {
          "args": {},
          "message": "ClusterMasterTest process has been killed.",
          "type": "WARN"
      }]
      """
