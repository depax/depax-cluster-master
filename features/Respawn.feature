@cluster-master @respawn
Feature: Cluster respawn

  Scenario: Should respawn workers if we get anything other than 99 exit code
    Given Create ClusterMaster using respawn workers
     Then I should have the following logs;
      """
      [{
          "args": {},
          "message": "Starting the master cluster and attempting to start 2 workers.",
          "type": "INFO"
      }, {
          "args": {},
          "message": "Received a 0 exit code, attempting to respawn the worker.",
          "type": "INFO"
      }, {
          "args": {},
          "message": "Received a 0 exit code, attempting to respawn the worker.",
          "type": "INFO"
      }, {
          "args": {},
          "message": "Received a 0 exit code, attempting to respawn the worker.",
          "type": "INFO"
      }, {
          "args": {},
          "message": "Received a 0 exit code, attempting to respawn the worker.",
          "type": "INFO"
      }, {
          "args": {},
          "message": "Received a 0 exit code, attempting to respawn the worker.",
          "type": "INFO"
      }, {
          "args": {},
          "message": "Received a 0 exit code, attempting to respawn the worker.",
          "type": "INFO"
      }, {
          "args": {},
          "message": "Received a 0 exit code, attempting to respawn the worker.",
          "type": "INFO"
      }, {
          "args": {},
          "message": "Received a 0 exit code, attempting to respawn the worker.",
          "type": "INFO"
      }, {
          "args": {},
          "message": "Received a 0 exit code, attempting to respawn the worker.",
          "type": "INFO"
      }, {
          "args": {},
          "message": "Received a 0 exit code, attempting to respawn the worker.",
          "type": "INFO"
      }, {
          "args": {},
          "message": "We have exceeded the max number of attempts with the interval.",
          "type": "INFO"
      }, {
          "args": {},
          "message": "Received a 0 exit code, attempting to respawn the worker.",
          "type": "INFO"
      }, {
          "args": {},
          "message": "We have exceeded the max number of attempts with the interval.",
          "type": "INFO"
      }, {
          "args": {},
          "message": "Received a 0 exit code, attempting to respawn the worker.",
          "type": "INFO"
      }, {
          "args": {},
          "message": "We have exceeded the max number of attempts with the interval.",
          "type": "INFO"
      }, {
          "args": {},
          "message": "Received a 0 exit code, attempting to respawn the worker.",
          "type": "INFO"
      }, {
          "args": {},
          "message": "We have exceeded the max number of attempts with the interval.",
          "type": "INFO"
      }, {
          "args": {},
          "message": "Received a 0 exit code, attempting to respawn the worker.",
          "type": "INFO"
      }, {
          "args": {},
          "message": "We have exceeded the max number of attempts with the interval.",
          "type": "INFO"
      }, {
          "args": {},
          "message": "Received a 0 exit code, attempting to respawn the worker.",
          "type": "INFO"
      }, {
          "args": {},
          "message": "We have exceeded the max number of attempts with the interval.",
          "type": "INFO"
      }, {
          "args": {},
          "message": "Received a 0 exit code, attempting to respawn the worker.",
          "type": "INFO"
      }, {
          "args": {},
          "message": "We have exceeded the max number of attempts with the interval.",
          "type": "INFO"
      }, {
          "args": {},
          "message": "Received a 0 exit code, attempting to respawn the worker.",
          "type": "INFO"
      }, {
          "args": {},
          "message": "We have exceeded the max number of attempts with the interval.",
          "type": "INFO"
      }, {
          "args": {},
          "message": "Received a 0 exit code, attempting to respawn the worker.",
          "type": "INFO"
      }, {
          "args": {},
          "message": "We have exceeded the max number of attempts with the interval.",
          "type": "INFO"
      }, {
          "args": {},
          "message": "Received a 0 exit code, attempting to respawn the worker.",
          "type": "INFO"
      }, {
          "args": {},
          "message": "We have exceeded the max number of attempts with the interval.",
          "type": "INFO"
      }, {
          "args": {},
          "message": "Received a 0 exit code, attempting to respawn the worker.",
          "type": "INFO"
      }, {
          "args": {},
          "message": "We have exceeded the max number of attempts with the interval.",
          "type": "INFO"
      }]
      """
