@cluster-master @shutdown
Feature: Cluster shutdown

  Scenario: Should respawn workers if we get anything other than 99 exit code
    Given Create ClusterMaster using shutdown workers
     Then I should have the following logs;
      """
      [{
          "args": {},
          "message": "Starting the master cluster and attempting to start 2 workers.",
          "type": "INFO"
      }, {
          "args": {},
          "message": "Received a shutdown message.",
          "type": "INFO"
      }, {
          "args": {},
          "message": "ClusterMasterTest process has been killed.",
          "type": "WARN"
      }, {
          "args": {},
          "message": "Received a shutdown message.",
          "type": "INFO"
      }, {
          "args": {},
          "message": "ClusterMasterTest process has been killed.",
          "type": "WARN"
      }]
      """
