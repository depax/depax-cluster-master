/**
 * Provides a test worker which will fire out several logs to the master.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

const ConsoleLogger = require("@depax/logger").ConsoleLogger;
const ClusterMaster = require("../../dist");

ClusterMaster.default("ClusterMasterTest", {
    singleWorkerDev: false,
}, async (config, logger) => {
    logger.isDebugging = true;

    await logger.info("Hello world, info from a worker.");
    await logger.warn("Hello world, warn from a worker.");
    await logger.error("Hello world, error from a worker.");
    await logger.fatal("Hello world, fatal from a worker.");
    await logger.debug("Hello world, debug from a worker.");

    process.exit(99);
}, async (config) => {
    const logger = new ConsoleLogger("master");
    return logger;
});
