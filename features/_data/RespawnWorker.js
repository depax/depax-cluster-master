/**
 * Provides a test worker which will trigger an exit, and should force a respawn.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

const ConsoleLogger = require("@depax/logger").ConsoleLogger;
const ClusterMaster = require("../../dist");

ClusterMaster.default("ClusterMasterTest", {
    singleWorkerDev: false,
}, async (config, logger) => {
    process.exit(0);
}, async (config) => {
    const logger = new ConsoleLogger("master");
    return logger;
});
