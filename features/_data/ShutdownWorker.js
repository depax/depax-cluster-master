/**
 * Provides a test worker which will fire a message to shutdown cluster.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

const ConsoleLogger = require("@depax/logger").ConsoleLogger;
const ClusterMaster = require("../../dist");

const sleep = (seconds) => new Promise((resolve) => setTimeout(resolve, seconds * 1000));

ClusterMaster.default("ClusterMasterTest", {
    singleWorkerDev: false,
}, async (config, logger) => {
    process.send("shutdown");
    await sleep(60 * 60);
}, async (config) => {
    const logger = new ConsoleLogger("master");
    return logger;
});
