/**
 * Provides the step definitions for the feature tests.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import Logger, { MemoryLogger } from "@depax/logger";
import chai = require("chai");
import cluster = require("cluster");
import { After, Given, Then, When } from "cucumber";
import os = require("os");
import path = require("path");
import { ClusterMaster } from "../../src";

const killWorker = (id: string): Promise<void> => new Promise((resolve) => {
    const worker = cluster.workers[id];

    worker.send("shutdown");
    worker.disconnect();

    setTimeout(() => {
        worker.kill();
        resolve();
    }, 2000);
});

const sleep = (seconds: number): Promise<void> => new Promise((resolve) => setTimeout(resolve, seconds * 1000));
let logger: Logger;
let master: ClusterMaster;

//#region Define the Before and After steps
After(async (): Promise<void> => {
    /* tslint:disable-next-line:forin */
    for (const id in cluster.workers) {
        await killWorker(id);
    }
});
//#endregion

//#region ----< Define the *Given* steps >------------------------------------------------------------------------------
Given(/^Create ClusterMaster using (logging|respawn|shutdown) workers$/, { timeout: -1 }, async (
    workerType: string,
): Promise<void> => {
    let exec: string;
    if (workerType === "logging") {
        exec = path.join(__dirname, "..", "_data", "LogWorker.js");
    } else if (workerType === "respawn") {
        exec = path.join(__dirname, "..", "_data", "RespawnWorker.js");
    } else if (workerType === "shutdown") {
        exec = path.join(__dirname, "..", "_data", "ShutdownWorker.js");
    }

    cluster.setupMaster({ exec });

    logger = new MemoryLogger("test");
    logger.isDebugging = true;

    await logger.initialize();

    master = new ClusterMaster({
        initializeForkDelay: 1500,
        maxForks: 2,
        title: "ClusterMasterTest",
    }, logger);

    await master.initialize();
});
//#endregion

//#region ----< Define the *When* steps >-------------------------------------------------------------------------------
When(/^I wait for ([0-9]*) seconds$/, async (
    seconds: number,
): Promise<void> => {
    await sleep(seconds);
});
// #endregion

//#region ----< Define the *Then* steps >-------------------------------------------------------------------------------
Then(/^I should have the following logs;$/, async (
    output: string,
): Promise<void> => {
    const logs = (logger as MemoryLogger).getLogs;
    logs.forEach((log) => delete log.date);

    chai.assert.deepEqual(logs, JSON.parse(output));
});
//#endregion
