/**
 * Provides the entry point for clustering.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import Logger, { ConsoleLogger, LogType, NullLogger } from "@depax/logger";
import cluster = require("cluster");
import {
    Builds,
    DefaultClusterMasterConfig,
    IClusterMasterConfig,
    InitializeCallback,
    InitializeMasterCallback,
} from "./";

/* istanbul ignore next */

/**
 * Provides a clustering entry point to handle creating the master cluster, and initializing the project.
 *
 * @param title This will be the process title of the master process.
 * @param config The configuration object.
 * @param initialize The initialize callback used in the worker to allow the caller to initialize the project.
 * @param initializeMaster The initialize callback used in the master.
 */
export default async (
    title: string,
    config: IClusterMasterConfig,
    initialize: InitializeCallback,
    initializeMaster?: InitializeMasterCallback,
) => {
    config = Object.assign({}, DefaultClusterMasterConfig, config);
    config.title = config.title || title;

    // If we are not in the master process, or the build is in dev mode, we want to initialize the application,
    // otherwise we want to initialize the cluster master class and let it handle forking, etc.
    if (!cluster.isMaster || (config.build === Builds.Dev && config.singleWorkerDev)) {
        let logger;
        if (config.build === Builds.Dev) {
            process.title = config.title;
        }

        if (config.build === Builds.Dev && config.singleWorkerDev) {
            logger = await initializeMaster(config);
            if (!logger) {
                logger = new ConsoleLogger("worker");
                await logger.initialize();
            }
        } else {
            const sendLog = (type: LogType, msg: string, args: any) => process.send({ args, cmd: "log", msg, type });

            // Setup the worker logger that will forward all logs to the cluster master.
            logger = new NullLogger("worker");
            logger
                .on(LogType.Info, (ctx) => sendLog(LogType.Info, ctx.message, ctx.args))
                .on(LogType.Warning, (ctx) => sendLog(LogType.Warning, ctx.message, ctx.args))
                .on(LogType.Error, (ctx) => sendLog(LogType.Error, ctx.message, ctx.args))
                .on(LogType.Fatal, (ctx) => sendLog(LogType.Fatal, ctx.message, ctx.args))
                .on(LogType.Debug, (ctx) => sendLog(LogType.Debug, ctx.message, ctx.args));

            process.on("message", (msg) => {
                // Check for a shutdown message.
                if (msg === "shutdown" || msg.cmd === "shutdown") {
                    process.exit(99);
                }
            });
        }

        // Run the provided initialize callback with the config and worker logger.
        try {
            await initialize(config, logger);
        } catch (err) {
            logger.fatal(err.message);
            process.exit(99);
        }
    } else {
        try {
            const logger = await initializeMaster(config);

            // We are in the master instance, create the ClusterMaster to manage the forks, etc.
            const ClusterMaster = require("./").ClusterMaster;
            const app = new ClusterMaster(config, logger);
            await app.initialize();
        } catch (err) {
            console.error("There was an error while initializing the master", err);
            process.exit(99);
        }
    }
};
