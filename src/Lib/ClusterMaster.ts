/**
 * Provides the cluster master class to help manage clustering in the engine and paxlets.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import Logger from "@depax/logger";
import cluster = require("cluster");
import os = require("os");

export declare type InitializeCallback = (config: IClusterMasterConfig, logger: Logger) => Promise<void>;
export declare type InitializeMasterCallback = (config: IClusterMasterConfig) => Promise<Logger | void>;

export enum Builds {
    /** The cluster is in DEV mode, so clustering will not be used. */
    Dev = "dev",

    /** The cluster is in TEST mode, so clustering is used. */
    Test = "test",

    /** The cluster is in LIVE mode, so clustering is used. */
    Live = "live",
}

export interface IClusterMasterConfig {
    /** The title of the main process. */
    title?: string;

    /** The build type. */
    build?: Builds;

    /** The maximum number of forks. */
    maxForks?: number;

    /** Set to true to use only master if in dev mode. */
    singleWorkerDev?: boolean;

    /** The delay before a new fork is created during the initialization. */
    initializeForkDelay?: number;

    /** The number of attempts we can respawn workers and fails, this will be multiplied by the max forks. */
    respawnAttempts?: number;

    /** The amount of time in ms before the attempts are cleared. */
    respawnAttemptsInterval?: number;
}

export const DefaultClusterMasterConfig: IClusterMasterConfig = {
    build: process.env.NODE_ENV === "production" ? Builds.Live : Builds.Dev,
    initializeForkDelay: 0,
    maxForks: os.cpus().length,
    respawnAttempts: 5,
    respawnAttemptsInterval: 1000,
    singleWorkerDev: true,
};

export default class ClusterMaster {
    /** The number of attempts to fork a worker. */
    protected attempts: number = 0;

    /**
     * Construct the cluster master.
     */
    public constructor(
        protected config: IClusterMasterConfig,
        protected logger: Logger,
    ) {
        this.config = Object.assign({}, DefaultClusterMasterConfig, this.config);

        process.title = config.title;
        this.config.maxForks = Math.max(1, Math.min(config.maxForks, os.cpus().length));

        cluster.on("exit", (worker, code, signal) => this.onClusterExit(worker, code, signal));
        cluster.on("message", (worker, msg) => this.onClusterMessage(worker, msg));

        setTimeout(() => {
            if (this.attempts > 0) {
                this.attempts--;
            }
        }, this.config.respawnAttemptsInterval);
    }

    /**
     * Initialize the cluster master.
     */
    public async initialize(): Promise<void> {
        if (this.logger) {
            this.logger.info(`Starting the master cluster and attempting to start ${this.config.maxForks} workers.`);
        }

        // Initialize a configured number of forks and limited to the number of CPUs available.
        for (let i = 0; i < this.config.maxForks; i++) {
            this.fork();
            await this.sleep(this.config.initializeForkDelay);
        }
    }

    /**
     * Create a new fork.
     */
    protected fork(): void {
        // Check if we have run out of attempts within the given interval period.
        if (this.attempts >= this.config.respawnAttempts * this.config.maxForks) {
            if (this.logger) {
                this.logger.info("We have exceeded the max number of attempts with the interval.");
            }

            return;
        }

        this.attempts++;
        cluster.fork();
    }

    /**
     * The cluster exit event listener.
     *
     * @param worker The cluster worker.
     * @param code The exit code, if it exited normally.
     * @param signal The name of the signal (e.g. 'SIGHUP') that caused the process to be killed.
     */
    protected onClusterExit(worker: cluster.Worker, code: number, signal: string): void {
        if (code === 99) {
            if (this.logger) {
                this.logger.warn(`${this.config.title} process has been killed.`);
            }

            return;
        }

        if (this.logger) {
            this.logger.info(`Received a ${code} exit code, attempting to respawn the worker.`);
        }

        this.fork();
    }

    /**
     * The cluster message event listener.
     *
     * @param msg The message recieved.
     */
    protected onClusterMessage(worker: cluster.Worker, msg: any): void {
        if (msg.cmd === "log" && this.logger) {
            const args = msg.args || {};
            args.worker = worker.id;

            this.logger.log(msg.type, msg.msg, args);
        } else if (msg === "shutdown" || msg.cmd === "shutdown") {
            if (this.logger) {
                this.logger.info("Received a shutdown message.");
            }

            /* tslint:disable-next-line:forin */
            for (const id in cluster.workers) {
                const wrkr = cluster.workers[id];
                wrkr.send("shutdown");
                wrkr.disconnect();

                setTimeout(() => wrkr.kill(), 2000);
            }
        }
    }

    /**
     * A simple util function to sleep/delay for a given amount of time.
     *
     * @param ms The number of millieseconds.
     */
    protected sleep(ms: number): Promise<void> {
        return new Promise((resolve) => setTimeout(resolve, ms));
    }
}
