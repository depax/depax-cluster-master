/**
 * Provides the route file of the module exposing all the relevant classes.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import EntryPoint from "./EntryPoint";
import ClusterMaster, {
    Builds,
    DefaultClusterMasterConfig,
    IClusterMasterConfig,
    InitializeCallback,
    InitializeMasterCallback,
} from "./Lib/ClusterMaster";

export default EntryPoint;
export {
    Builds,
    ClusterMaster,
    DefaultClusterMasterConfig,
    IClusterMasterConfig,
    InitializeCallback,
    InitializeMasterCallback,
};
